<?php   
    class Table extends CI_Model{

        public function__construct(){
            $this->load->database();
        }

        public function get_all(){
            $donnee = $this->db->get('table_restaurant');
            return $donnee->result_array();
        }
    }