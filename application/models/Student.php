<?php
     class Student extends CI_Model{

          public function __construct(){
               $this->load->database();
          }
          public function get_all(){
               $query = $this->db->get('student');
               return $query->result_array();
          }
     }
?>