<?php
     class Dishes extends CI_Model{

          public function __construct(){
               $this->load->database();
          }
          public function get_all(){
               $query = $this->db->get('dishes');
               return $query->result_array();
          }
     }
?>