<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dishes_controller extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
    } 
	
	public function index()
	{
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('Dishes');
        $data['dishes'] = $this->Dishes->get_all(); 
        $this->load->view('Accueil',$data);
    }
    
}