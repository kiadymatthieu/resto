<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Hello extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->model('Student');
                $this->load->helper('url_helper');
        }
        public function index(){
            $data['students'] = $this->Student->get_all();
            $data['view']='home.php';
            $this->load->view('template.php',$data);
        }
    }