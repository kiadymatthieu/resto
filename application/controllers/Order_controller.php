<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_controller extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
    } 
	
	public function index()
	{
        $this->load->helper('form');
        $this->load->helper('url');
        $idDish = $this->input->get('idDish');
        $data['tables'] = $this->db->select('Orders');
        echo $idDish;
        $this->load->view('pages/home',$idDish,$data);
        //mi-insert commande pour une table (into Orders)
        //$this->load->view('login'); //redirection vers commande reçu et en attente 
        
    }
    
    
}