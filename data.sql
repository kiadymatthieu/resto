--mysql
CREATE DATABASE food;
use food;

-- table front
 --table plat
 -- commande
 --commande par table
 -- -- -- -- -- 
 -- paiement
 -- 
--table categorie plat
CREATE TABLE Tables(
    idTable VARCHAR(20) primary key
);
INSERT INTO Tables VALUES('1');

CREATE TABLE Payments(
    idPayment VARCHAR(20) primary key,
    idTable VARCHAR(20),
    datePayment TIMESTAMP,
    sums INTEGER,
    FOREIGN KEY (idTable) REFERENCES Tables(idTable)
); 
INSERT INTO Payments VALUES('','','',);

CREATE TABLE Orders(
    idOrder VARCHAR(20),
    idDish VARCHAR(20),
    idTable VARCHAR(20),
    FOREIGN KEY (idDish) REFERENCES Dishes(idDish),
    FOREIGN KEY (idTable) REFERENCES Tables(idTable)
);
INSERT INTO Orders VALUES('','','');

CREATE table Dishes(
    idDish VARCHAR(20),
    name VARCHAR(30),
    description VARCHAR(100),
    category VARCHAR(20),
    picture VARCHAR(50),
    price INTEGER
);
INSERT INTO Dishes VALUES('','','','','',);
INSERT INTO Dishes VALUES('plat1','mi-sao simple','(pâtes,légumes,crudités)','pates','',5000);
Query OK, 1 row affected (0.07 sec)

mysql> INSERT INTO Dishes VALUES('plat2','soupe chinoise','(pâtes,van tan,oeuf)','soupes','',3000);
Query OK, 1 row affected (0.00 sec)

CREATE TABLE Waiters(
    idWaiter VARCHAR(20) PRIMARY KEY,
    userName VARCHAR(50),
    password VARCHAR(50)  -- type utilisateur exemple 
);
INSERT INTO Waiters VALUES('','','');
/*
Plat taloha
CREATE table Plat(
    idPlat SERIAL NOT NULL,
    nom VARCHAR(50),
    nombre INTEGER,
    prix INTEGER
);
*/
/**
INSERT INTO table_restaurant VALUES('1');
INSERT INTO table_restaurant VALUES('2');
INSERT INTO table_restaurant VALUES('3');
INSERT INTO table_restaurant VALUES('4');
INSERT INTO table_restaurant VALUES('5');
INSERT INTO table_restaurant VALUES('6');
INSERT INTO table_restaurant VALUES('7');
INSERT INTO table_restaurant VALUES('8');
INSERT INTO table_restaurant VALUES('9');
INSERT INTO table_restaurant VALUES('10');
INSERT INTO table_restaurant VALUES('11');


ALTER TABLE Paiement ADD PRIMARY KEY(idPaiement);
ALTER TABLE Paiement ADD FOREIGN KEY(idCommandeParTable) REFERENCES commandeParTable(idCommandeParTable);  


ALTER TABLE commande ADD PRIMARY KEY(idCommande);
ALTER TABLE commande ADD FOREIGN KEY(idPlat) REFERENCES Plat(idPlat);  
/* mety tsy ilaina
CREATE TABLE commandeParTable(
    idCommandeParTable SERIAL NOT NULL,
    idCommande SERIAL NOT NULL,
    idTable SERIAL NOT NULL
);
*/
/*
ALTER TABLE commandeParTable ADD PRIMARY KEY(idCommandeParTable);
ALTER TABLE commandeParTable ADD FOREIGN KEY(idCommande) REFERENCES commande(idCommande);   
ALTER TABLE commandeParTable ADD FOREIGN KEY(idTable) REFERENCES table_restaurant(idTable_restaurant);   


ALTER TABLE Plat ADD PRIMARY KEY(idPlat);

ALTER TABLE Paiement ADD FOREIGN KEY(idCommandeParTable) REFERENCES commandeParTable(idCommandeParTable); 
ALTER TABLE commandeParTable ADD FOREIGN KEY(idCommande) REFERENCES commande(idCommande);  
ALTER TABLE commandeParTable ADD FOREIGN KEY(idTable) REFERENCES table_restaurant(idTable_restaurant); 


CREATE TABLE categoriePlat(
    idCategoriePlat SERIAL NOT NULL,
    nom VARCHAR(50)
);
ALTER TABLE categoriePlat ADD PRIMARY KEY(idCategoriePlat);

--table back
CREATE TABLE Users(
    idUsers SERIAL NOT NULL,
    login VARCHAR(50),
    password VARCHAR(80),
    idProfile SERIAL  -- type utilisateur exemple 
);

CREATE TABLE Profile(
    idProfile SERIAL NOT NULL,
    name VARCHAR(50)
);